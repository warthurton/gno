/*
 * Too bad we don't have symlinks for GNO ...
 *
 * $Id: file.h 291 1997-10-30 02:13:59Z gdr $
 */

#ifndef _SYS_FILE_H_
#define _SYS_FILE_H_

/* <sys/file.h> is the old BSD name for <sys/fcntl.h> */

#ifndef _SYS_FCNTL_H_
#include <sys/fcntl.h>
#endif

#endif
