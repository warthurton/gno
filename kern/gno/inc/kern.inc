*	$Id: kern.inc 432 1998-02-02 08:20:59Z taubert $
* These equates are used in code that calls setHoldSig.

hsHoldSig	gequ  1
hsNoHoldSig	gequ  0

FL_SELECTING	gequ  $100

PS_SLEEP	gequ  9
