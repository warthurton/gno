#
# gno/lib/libc/rules.mk
#
# Default rules for libc (override of /usr/local/lib/startup.mk).
#
# Devin Reade, 1997
#
# $Id: rules.mk 156 1997-09-21 16:22:23Z gdr $
#

%.o: %.c
	$(CC) -o $@ -c $(__OFLAG) $(CFLAGS) $<

%.o: %.asm
	$(AS) -o $@ -c $(__OFLAG) $(ASFLAGS) $<
	@$(RM) $*.root

../libc .PHONY: $(OBJS)
	$(MAKELIB) $(MAKELIBFLAGS) -l $@ $(OBJS)
              
