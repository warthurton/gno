/*
 * $Id: gno.c 46 1997-02-28 05:12:58Z gdr $
 */
#include "test.h"

#include <gno/conf.h>
#include <gno/gno.h>
#include <gno/proc.h>
#include <gno/kvm.h>
#include <gno/sim.h>

/* haven't yet determined if it will be dropped */
#if 0
#include <gno/kerntool.h>
#endif
