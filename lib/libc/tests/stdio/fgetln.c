/*
 * Tests fgetln(3) routine -- interactively.
 * 
 * Devin Reade, 1997.
 * 
 * $Id: fgetln.c 79 1997-07-27 23:50:59Z gdr $
 */

#include <stdio.h>

int 
main(int argc, char **argv) {
  char *p;
  size_t len;
  
  while ((p = fgetln(stdin, &len)) != NULL) {
    printf("%d:%s", len, p);
  }
  return 0;
}
